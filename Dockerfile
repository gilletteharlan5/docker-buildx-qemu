# Don't depend on Docker Hub
FROM registry.fedoraproject.org/fedora:38

# Add Docker CE repo
RUN dnf -y install dnf-plugins-core \
 && dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

# Install Docker
RUN dnf -y install \
        docker-ce-cli \
        docker-buildx-plugin 

# Install QEMU
RUN dnf -y install \
        qemu-user-binfmt \
        qemu-user-static 

# Install Git
RUN dnf -y install \
        git